package websockets;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by admin on 28.12.2015.
 */
public class MessageParser {
    public static String parseResponse(String jsonResp) {


        JSONObject jsonObj = new JSONObject(jsonResp);
        Iterator<String> keys = jsonObj.keys();

        while (keys.hasNext()) {
            if (keys.next().equals("shot")) {
                return "shot";
            }

        }

        return "none";

    }

    public static String prepareJson(String type, Object obj) {

        JSONObject jsonObj = new JSONObject();

        jsonObj.put(type,obj);

        return jsonObj.toString();


    }
    public static String prepareJson(String type, boolean obj) {

        JSONObject jsonObj = new JSONObject();

        jsonObj.put(type,obj);

        return jsonObj.toString();


    }
}
