package websockets;


import logic.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.*;


import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import java.io.PrintWriter;


/**
 * Created by admin on 19.12.2015.
 */
@ServerEndpoint("/pc")
public class PlayerConnection {

    private static final String GUEST_PREFIX = "Admiral";
    private static final AtomicInteger connectionIds = new AtomicInteger(0);
    private static final List<PlayerConnection> listOfWaitings = new CopyOnWriteArrayList<PlayerConnection>();
    public final String nickname;
    private final Logger logger = Logger.getLogger(getClass().getName());
    public PlayerConnection opponent;
    public boolean permissionOfMoving;
    public Board boardObj = new Board();
    private Session session;
    private int destroyed = 0;

    private MessageParser messageParser = new MessageParser();


    public PlayerConnection() {
        this.nickname = GUEST_PREFIX + connectionIds.getAndIncrement();
    }

    @OnOpen
    public void onOpen(Session session) throws IOException {
        this.session = session;
        if (listOfWaitings.isEmpty()) {
            listOfWaitings.add(this);
            permissionOfMoving = true; //��� ������ ������, ��� � �����.
            String jsonMessage = messageParser.prepareJson("message", "������� ����������");
            session.getBasicRemote().sendText(jsonMessage);
        } else {
            setOpponents(listOfWaitings.get(0));
            listOfWaitings.remove(0);
            permissionOfMoving = false;
            this.startGame();
            opponent.startGame();
            String jsonOurMessage = messageParser.prepareJson("message", "��� ���������");
            String jsonOpponentMessage = messageParser.prepareJson("message", "��� ���");
            session.getBasicRemote().sendText(jsonOurMessage);
            opponent.session.getBasicRemote().sendText(jsonOpponentMessage);

        }

        // session.getBasicRemote().sendText("nickname: " + nickname + " opponent: " + opponent);
    }


    @OnMessage
    public void incoming(String message) {
        /*if (opponent != null) {
            try {
                opponent.session.getBasicRemote().sendText(message + " from " + nickname);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }*/
        messageParser.parseResponse(message);

    }

    @OnError
    public void onError(Throwable t) {
        t.printStackTrace();

    }

    @OnClose
    public void onClose(Session session) throws IOException {

        if (opponent != null) {

            opponent.session.getBasicRemote().sendText("{\"message\" : \"��� ��������� ������� ����.\"}");
            opponent.session.close();
        } else {
            listOfWaitings.remove(this);
        }
    }

    public void setOpponents(PlayerConnection opponent) {
        this.opponent = opponent;
        opponent.opponent = this;
    }

    public void startGame() throws IOException {
        int[][] shipsLocation = boardObj.getShipsLocation();
        String jsonBoard = messageParser.prepareJson("mappingShips", shipsLocation);
        session.getBasicRemote().sendText(jsonBoard);
        String jsonStr = messageParser.prepareJson("permissionOfMoving", permissionOfMoving);
        session.getBasicRemote().sendText(jsonStr);
    }
    @Override
    public String toString() {
        return connectionIds + " " + nickname;

    }
    public void echo(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }
    public String getSessionId() {
        return this.session.getId();
    }
    private class MessageParser {
        public void parseResponse(String jsonResp) {
            JSONObject jsonOurMessage = new JSONObject();
            JSONObject jsonOpponentMessage = new JSONObject();
            JSONArray item = new JSONArray();

            JSONObject jsonObj = new JSONObject(jsonResp);
            Iterator<String> keys = jsonObj.keys();

            while (keys.hasNext()) {
                if (keys.next().equals("shot")) {
                    JSONArray arr = jsonObj.getJSONArray("shot");
                    int xCoord = arr.getInt(0);
                    int yCoord = arr.getInt(1);
                    Field field = opponent.boardObj.getBoard()[xCoord][yCoord];
                    if (field instanceof Ship) {
                        ((Ship) field).addHit();
                        item.put(0, true);
                        if (((Ship) field).isDestroyed()) {
                            item.put(1, true);
                            item.put(2, ((Ship) field).getLocation());
                            jsonOurMessage.put("alert", "��������� " + ((Ship) field).getShipName() + " ������.");
                            destroyed++;
                        } else {
                            item.put(1, false);
                            item.put(2, new int[]{xCoord, yCoord});
                        }
                        jsonOurMessage.put("shotResult", item);
                        jsonOpponentMessage.put("opponentStep", item);
                        if (destroyed == 10) {
                            jsonOurMessage.put("message", "�� ��������!");
                            jsonOpponentMessage.put("message", "��������� �������!");
                            permissionOfMoving = false;
                            opponent.permissionOfMoving = false;
                            jsonOurMessage.put("permissionOfMoving", false);
                            jsonOpponentMessage.put("permissionOfMoving", false);
                        } else {
                            jsonOurMessage.put("message", "��� ���");
                            jsonOpponentMessage.put("message", "��� ����������");
                        }
                    } else {

                        item.put(0, false);
                        item.put(1, false);
                        item.put(2, new int[]{xCoord, yCoord});
                        permissionOfMoving = false;
                        opponent.permissionOfMoving = true;
                        jsonOurMessage.put("shotResult", item);
                        jsonOpponentMessage.put("opponentStep", item);
                        jsonOurMessage.put("permissionOfMoving", false);
                        jsonOpponentMessage.put("permissionOfMoving", true);
                        jsonOurMessage.put("message", "��� ����������");
                        jsonOpponentMessage.put("message", "��� ���");
                    }

                    try {
                        session.getBasicRemote().sendText(jsonOurMessage.toString());
                        opponent.session.getBasicRemote().sendText(jsonOpponentMessage.toString());
                    } catch (IOException e) {
                    }
                }
            }
        }

        public String prepareJson(String type, Object obj) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put(type, obj);
            return jsonObj.toString();
        }

        public String prepareJson(String type, boolean obj) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put(type, obj);
            return jsonObj.toString();
        }
    }
}


