package logic;


import java.util.ArrayList;

/**
 * Created by admin on 20.12.2015.
 */


public class Ship implements Field {

    private final int orientation;
    private int numberOfDecks;
    private int numberOfBombingDecks;
    private String shipName;

    private ArrayList <String> location = new ArrayList<String>();


    public Ship(int numberOfDecks) {
        this.orientation = (int) Math.round(Math.random());
        this.numberOfDecks = numberOfDecks;
        this.shipName = ShipsConstants.SHIPSNAMES[numberOfDecks];
        this.numberOfBombingDecks = 0;
    }


    public Ship(int numberOfDecks, int orientation) {
        this.orientation = orientation;
        this.numberOfDecks = numberOfDecks;
        this.shipName = ShipsConstants.SHIPSNAMES[numberOfDecks];
        this.numberOfBombingDecks = 0;
    }

    public static Ship createShip(int numberOfDecks) {
        return new Ship(numberOfDecks);
    }

    public void resetLocation () {
        location.clear();
    }

    public String getShipName() {
        return shipName;
    }

    public void setLocation (int xCoord,int yCoord) {
        location.add(xCoord + "" + yCoord);
    }

    public String [] getLocation () {
        String [] locaArr = new String[location.size()];
        location.toArray(locaArr);
        return locaArr;
    }

    public int getOrientation() {
        return orientation;
    }

    public void addHit() {
        this.numberOfBombingDecks++;
    }

    public boolean isDestroyed() {
        return ((numberOfDecks - numberOfBombingDecks) == 0);
    }

    public int getNumberOfDecks() {
        return numberOfDecks;
    }

    @Override
    public String toString() {
        return "SHIP";
    }
}

