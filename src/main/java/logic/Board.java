package logic;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 20.12.2015.
 * Создаем игровую доску.
 * создаем экземпляры кораблей.
 * заполняем игровую доску.
 */
public class Board {

    static final int BOARDSIZE = 10;
    static final FieldNearShip FIELDNEARSHIP = new FieldNearShip();


    private Field[][] board = new Field[BOARDSIZE][BOARDSIZE];


    public Board() {
        fillBoard();
    }


    /**
     * сначала проверяем свободен ли предложенный вариант для конкретного корабля, а потом, если свободен, заполняем клетки ссылками на объект-корабль;
     */
    private void fillBoard() {

        for (int numberOfShipsCurrModif = 1; numberOfShipsCurrModif < ShipsConstants.SHIPSARRAY.length; numberOfShipsCurrModif++) {
            for (int jIter = 1; jIter <= numberOfShipsCurrModif; jIter++) {
                Ship ship = Ship.createShip(ShipsConstants.SHIPSARRAY[numberOfShipsCurrModif]);
                int[] coordinates = getRandomCoordinatesForShip(ship);
                while (!checkSpaceForShip(coordinates[0], coordinates[1], ship)) {
                    coordinates = getRandomCoordinatesForShip(ship);
                }
                locateShipOnBoard(coordinates[0], coordinates[1], ship);
            }
        }
    }


    public Field[][] getBoard() {
        return board;
    }

    public int[][] getShipsLocation() {
        int[][] shipsLocation = new int[20][2];

        int k = 0;
        for (int i = 0; i < BOARDSIZE; i++)
            for (int j = 0; j < BOARDSIZE; j++) {
                if ((board[i][j] != null) && (board[i][j] instanceof Ship)) {

                    shipsLocation[k] = new int[]{i, j};
                    k++;
                }
            }
        return (shipsLocation);
    }


    private boolean checkSpaceForShip(int xBoardPosition, int yBoardPosition, Ship ship) {
        switch (ship.getOrientation()) {
            case (ShipsConstants.VERTICAL_ORIENTATION):
                for (int i = yBoardPosition; i < yBoardPosition + ship.getNumberOfDecks(); i++) {
                    if (board[i][xBoardPosition] != null) {
                        return false;
                    }
                }
                break;
            case (ShipsConstants.HORIZONTAL_ORIENTATION):
                for (int i = xBoardPosition; i < xBoardPosition + ship.getNumberOfDecks(); i++) {
                    if (board[yBoardPosition][i] != null) {
                        return false;
                    }
                }
                break;
        }
        return true;
    }

    private void locateShipOnBoard(int xBoardPosition, int yBoardPosition, Ship ship) {
        switch (ship.getOrientation()) {
            case (ShipsConstants.VERTICAL_ORIENTATION):
                for (int i = yBoardPosition - 1; i < yBoardPosition + ship.getNumberOfDecks() + 1; i++) {
                    for (int j = xBoardPosition - 1; j <= xBoardPosition + 1; j++) {
                        if (i < 0 || j < 0 || (i > BOARDSIZE - 1) || (j > BOARDSIZE - 1)) continue;
                        if (j != xBoardPosition || i < yBoardPosition || i > (yBoardPosition + ship.getNumberOfDecks() - 1)) {
                            board[i][j] = FIELDNEARSHIP;
                        } else {
                            board[i][j] = ship;
                            ship.setLocation(i,j);
                        }
                    }

                }
                break;
            case (ShipsConstants.HORIZONTAL_ORIENTATION):

                for (int i = xBoardPosition - 1; i < xBoardPosition + ship.getNumberOfDecks() + 1; i++) {
                    for (int j = yBoardPosition - 1; j <= yBoardPosition + 1; j++) {

                        if (i < 0 || j < 0 || (i > BOARDSIZE - 1) || (j > BOARDSIZE - 1)) continue;
                        if (j != yBoardPosition || i < xBoardPosition || i > (xBoardPosition + ship.getNumberOfDecks() - 1)) {
                            board[j][i] = FIELDNEARSHIP;
                        } else {
                            board[j][i] = ship;
                            ship.setLocation(j,i);
                        }
                    }
                }
                break;
        }
    }


    private int[] getRandomCoordinatesForShip(Ship ship) {
        int xBoardPosition = 0;
        int yBoardPosition = 0;
        switch (ship.getOrientation()) {
            case (ShipsConstants.VERTICAL_ORIENTATION):
                xBoardPosition = (int) Math.round(Math.random() * (BOARDSIZE - 1));
                yBoardPosition = (int) Math.round(Math.random() * (BOARDSIZE - ship.getNumberOfDecks()));
                break;
            case (ShipsConstants.HORIZONTAL_ORIENTATION):
                xBoardPosition = (int) Math.round(Math.random() * (BOARDSIZE - ship.getNumberOfDecks()));
                yBoardPosition = (int) Math.round(Math.random() * (BOARDSIZE - 1));
                break;
        }
        return new int[]{xBoardPosition, yBoardPosition};

    }
}
