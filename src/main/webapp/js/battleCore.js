/**
 * Created by admin on 23.12.2015.
 */

//var wsocket=0;

(function ($) {
    "use strict";
    function GAME() {


        /*private members*/

        var wsocket,
            host = (location.host == "localhost:8080") ? "ws://" + location.host + location.pathname + "pc" : "ws://" + location.host + ":8000" + location.pathname + "pc",
            permissionOfMoving = false,
            init = function () {
                connectToServer();
                createGameTable("our");
                createGameTable("opponent");
                createTableListeners();
            },
            createGameTable = function (ID) {
                var tbl = document.createElement('table');
                tbl.setAttribute("id", ID + "Table");

                for (var i = 0; i < 10; i++) {
                    var tr = document.createElement('tr');
                    tbl.appendChild(tr);
                    for (var j = 0; j < 10; j++) {
                        var td = document.createElement('td');
                        td.setAttribute("id", ID + "" + i + '' + j);
                        td.classList.add(ID + "Td");
                        tr.appendChild(td);
                    }
                }
                var div = document.getElementById("content");
                div.appendChild(tbl);
            },
            createTableListeners = function () {

                /*[].forEach.call(document.querySelectorAll('.opponentTd'), function (el) {
                 el.addEventListener('click', function () {
                 var coord = el.getAttribute("id");
                 console.log(coord);
                 console.log(permissionOfMoving);
                 console.log(coord[coord.length-2]+" "+coord[coord.length-1]);
                 if (permissionOfMoving) {

                 doShot(coord[coord.length-2], coord[coord.length-1]);
                 }
                 }, false);
                 });*/
                $(".opponentTd").on("click", function () {
                    var coord = this.getAttribute("id");
                    console.log(coord);
                    console.log(permissionOfMoving);
                    console.log(coord[coord.length - 2] + " " + coord[coord.length - 1]);
                    if (permissionOfMoving) {

                        doShot(coord[coord.length - 2], coord[coord.length - 1]);
                        $(this).off();
                    }
                })
            },
            resetGameTable = function () { //timely unused
            },
            startGame = function () {
            },
            finishGame = function () {

            },

            interruptGame = function () {

            },
            parseServerResponse = function (resp) {

                var response = JSON.parse(resp.data);
                console.log("response", response);

                for (var property in response) if (response.hasOwnProperty(property)) {
                    switch (property) {
                        case "message":
                            showMesssage(response[property]);
                            console.log(property, response[property]);
                            break;
                        case "alert":
                            showAlert(response[property]);
                            console.log(property, response[property]);
                            break;
                        case "opponentStep":
                            opponentShot(response[property]);
                            console.log(property, response[property]);
                            break;
                        case "shotResult":
                            shotResult(response[property]);
                            console.log(property, response[property]);
                            break;
                        case "mappingShips":
                            mappingShips(response[property]);
                            console.log(property, response[property]);
                            break;
                        case "permissionOfMoving":
                            setPermissionOfmoving(response[property]);
                            break;
                    }
                }


            },
            setPermissionOfmoving = function (permission) {
                permissionOfMoving = permission;
                var opponentTable = document.getElementById("opponentTable");
                opponentTable.setAttribute("class", "");
                switch (permission) {
                    case true:
                        opponentTable.classList.add("active");
                        break;
                    case false:
                        opponentTable.classList.remove("active");
                        break;
                }
            },
            showMesssage = function (message) {
                var el = document.getElementById("response");
                var innerEl = el.firstElementChild;
                innerEl.innerHTML = message;
            },
            showAlert = function (alertMessage) {
                var el = document.getElementById("alert");
                var innerEl = el.firstElementChild;
                innerEl.innerHTML = alertMessage;
                el.style.visibility = "visible";
                setTimeout(function () {
                    el.style.visibility = "hidden"
                }, 3000);
            },
            connectToServer = function () {
                if (!wsocket) {
                    wsocket = new WebSocket(host);
                    wsocket.onmessage = parseServerResponse;
                    wsocket.onerror = function (e) {
                        $("#response").html("ошибка");
                        console.log(e.data);
                    }
                }
            },
            opponentShot = function (respObj) {
                var isHit = respObj[0],
                    isHitFullShip = respObj[1],
                    coordArrayOfFullShip = respObj[2],
                    coordX,
                    coordY,
                    td;
                switch (isHit) {
                    case true:
                        if (isHitFullShip) {
                            for (var c in coordArrayOfFullShip) if (coordArrayOfFullShip.hasOwnProperty(c)) {
                                coordX = coordArrayOfFullShip[c][0];
                                coordY = coordArrayOfFullShip[c][1];
                                td = document.getElementById("our" + coordX + "" + coordY);
                                td.setAttribute("class", "");
                                td.classList.add("destroyed");
                            }
                        } else {
                            coordX = coordArrayOfFullShip[0];
                            coordY = coordArrayOfFullShip[1];
                            td = document.getElementById("our" + coordX + "" + coordY);
                            td.setAttribute("class", "");
                            td.classList.add("burning");
                        }
                        break;
                    case false:
                        coordX = coordArrayOfFullShip[0];
                        coordY = coordArrayOfFullShip[1];
                        td = document.getElementById("our" + coordX + "" + coordY);
                        td.classList.remove("ourTd");
                        td.classList.add("miss");
                        break;
                }
            },
            doShot = function (coordX, coordY) {
                var jsonObj = {
                    shot: [coordX, coordY]
                };
                var jsonStr = JSON.stringify(jsonObj);
                console.log(jsonStr);
                wsocket.send(jsonStr);
            },
            shotResult = function (resp) {
                var isHit = resp[0];
                var isHitFullShip = resp[1];
                var coordArrayOfFullShip = resp[2];
                var coordX;
                var coordY;
                var td;
                switch (isHit) {
                    case true:
                        if (isHitFullShip) {
                            for (var c in coordArrayOfFullShip) if (coordArrayOfFullShip.hasOwnProperty(c)) {
                                coordX = coordArrayOfFullShip[c][0];
                                coordY = coordArrayOfFullShip[c][1];
                                td = document.getElementById("opponent" + coordX + "" + coordY);
                                td.setAttribute("class", "");
                                td.classList.add("destroyed");
                            }
                        } else {
                            coordX = coordArrayOfFullShip[0];
                            coordY = coordArrayOfFullShip[1];
                            td = document.getElementById("opponent" + coordX + "" + coordY);
                            td.setAttribute("class", "");
                            td.classList.add("burning");
                        }
                        break;
                    case false:
                        coordX = coordArrayOfFullShip[0];
                        coordY = coordArrayOfFullShip[1];
                        td = document.getElementById("opponent" + coordX + "" + coordY);
                        td.classList.remove("opponentTd");
                        td.classList.add("miss");
                        break;
                }
            },
            mappingShips = function (coordObj) {
                var coordX;
                var coordY;
                var td;
                for (var c in coordObj) if (coordObj.hasOwnProperty(c)) {
                    coordX = coordObj[c][0];
                    coordY = coordObj[c][1];
                    td = document.getElementById("our" + coordX + "" + coordY);
                    td.classList.add("ship");
                }


            };


        /*----------------------------------------------------------------------------------------------*/
        init();
    }


    $(document).ready(function () {

        $("#connect").click(function () {
            var game = new GAME();
            $(this).unbind().bind("click", function () {
                location.reload();
            }).html("Начать заново.")
        });


    })


})(jQuery);
