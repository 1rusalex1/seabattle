package logic;


import org.junit.Before;
import org.junit.Test;


import java.lang.reflect.Constructor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by admin on 21.12.2015.
 */
public class TestBoardFactoryClass {
    private Board tested;

    @Before
    public void unitTestData() throws NoSuchMethodException {

        tested = new Board();


    }

/*    @Test
    public void testHowLocateShipOnBoardWork() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Ship ship = new Ship(4);

        Class classShip = ship.getClass();

        Class[] paramTypes = new Class[]{int.class, int.class};
        Constructor shipConstrct = classShip.getDeclaredConstructor(paramTypes);
        shipConstrct.setAccessible(true);

        ship = (Ship) shipConstrct.newInstance(4, ShipsConstants.HORIZONTAL_ORIENTATION);

        //       assertEquals(ShipsConstants.HORIZONTAL_ORIENTATION, ship.getOrientation());

        Class classTested = tested.getClass();
        Class[] params = new Class[]{int.class, int.class, Ship.class};
        Method method = classTested.getDeclaredMethod("locateShipOnBoard", params);
        method.setAccessible(true);

        Object[] args = new Object[]{0, 3, ship};
        method.invoke(tested, args);
        Field[][] board = tested.getBoard();

        assertTrue(board[1][0] == null);
        assertTrue(board[1][1] == null);
        assertTrue(board[1][2] == null);
        assertTrue(board[1][3] == null);
        assertTrue(board[1][4] == null);
        assertTrue(board[1][5] == null);
        assertTrue(board[2][5] == null);
        assertTrue(board[3][5] == null);
        assertTrue(board[4][5] == null);
        assertTrue(board[5][5] == null);
        assertTrue(board[5][4] == null);
        assertTrue(board[5][3] == null);
        assertTrue(board[5][2] == null);
        assertTrue(board[5][1] == null);
        assertTrue(board[5][0] == null);


        assertTrue(board[3][0] instanceof Ship);
        assertTrue(board[3][1] instanceof Ship);
        assertTrue(board[3][2] instanceof Ship);
        assertTrue(board[3][3] instanceof Ship);

        assertTrue(board[2][0] instanceof FieldNearShip);
        assertTrue(board[2][1] instanceof FieldNearShip);
        assertTrue(board[2][2] instanceof FieldNearShip);
        assertTrue(board[2][3] instanceof FieldNearShip);
        assertTrue(board[2][4] instanceof FieldNearShip);
        assertTrue(board[3][4] instanceof FieldNearShip);
        assertTrue(board[4][4] instanceof FieldNearShip);
        assertTrue(board[4][3] instanceof FieldNearShip);
        assertTrue(board[4][2] instanceof FieldNearShip);
        assertTrue(board[4][1] instanceof FieldNearShip);
        assertTrue(board[4][0] instanceof FieldNearShip);


        tested = new Board();

        tested.fillBoard();

        board = tested.getBoard();




        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }


    }*/
}
