package websockets;

import logic.Board;
import org.junit.Test;

/**
 * Created by admin on 29.12.2015.
 */
public class TestMessageParser {
    @Test
    public void testPrepareJsonWork () {
        Board boardObj = new Board();
        int [][] shipsLocation = boardObj.getShipsLocation();
        String jsonBoard = MessageParser.prepareJson("mappingShips",shipsLocation);
        System.out.println(jsonBoard);
    }
}
